<?php
/**
 * Created by PhpStorm.
 * User: ghedi
 * Date: 9/4/2017
 * Time: 4:07 PM
 */

namespace Gpws\Interfaces;


interface Message {


    public function getContents(): string;
    public function getConnection(): Connection;
}