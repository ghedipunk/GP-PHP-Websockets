<?php


namespace Gpws\Interfaces;


interface WebsocketStandard {
    public function frame(string $message): string;
    public function deframe(string $message): string;
    //protected function handshake(Connection $connection);
    public function setupNewConnection($resource): Connection;
}